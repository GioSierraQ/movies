# README #

Desarrollar una aplicación capaz de consultar la información de la API:
https://developers.themoviedb.org/3/getting-started/introduction.

Permitiendo:
● La visualización de la información de las películas más populares en el
servidor de TMDb.
● Acceder al detalle de la película.

### Que se entrega ###

* Buen uso de Object Oriented Programming
* Proyecto a partir de un repositorio en git, que sea público.
* Utilización del patron GitFlow
* App basado en el patrón de diseño VIPER
* Manejo de  los estados de carga sobre la vista
* Uso de xib y de storyboard
* Funcion de like en las pelicula mediante Core Data
* Integración de pruebas unitarias para algunos casos

### Puedes contactarme ###

* Desarrollador iOS
* Giovani Sierra Quintero
* 300 615 6057
* Cualquier duda o inquietud acerca de la prueba por favor escribir o llamar para resolverla.