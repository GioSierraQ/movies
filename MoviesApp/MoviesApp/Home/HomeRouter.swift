//
//  HomeRouter.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import UIKit

protocol HomeRouting {
    func goToDetail(movie: ModelDataDetail?)
    func goToLike()
}

class HomeRouter {
    
    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
    
}

extension HomeRouter: HomeRouting{

    func goToDetail(movie: ModelDataDetail?) {
        
        guard let movieR = movie else{
            return
        }
        let vc = DetailAssembly.buid(movie: movieR)
        self.view.navigationController?.pushViewController(vc, animated: true)
    }
    
    func goToLike() {
        let vc = LikeAssembly.build()
        self.view.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
