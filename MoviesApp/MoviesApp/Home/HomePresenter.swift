//
//  HomePresenter.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import Foundation

protocol Homepresentation {
    func viewDidLoad()
    func listMovies(page: Int)
    func prepareForRouter(movie: Result)
    func actionButtonRightBar()
}

class HomePresenter {
    
    weak var view: HomeView?
    var interactor: HomeUseCase
    var router: HomeRouting
        
    init(view: HomeView, interactor: HomeUseCase, router: HomeRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}

extension HomePresenter: Homepresentation{

    func viewDidLoad() {
        
    }
    
    func listMovies(page: Int) {
        
        self.interactor.fetchRequest(page: page, callBack: { (result) in
            
            switch result {
            
                case .success(let data):
                    guard let moviesTrending = data as? Trending else {
                        return
                    }
                    self.view?.updateMovies(dataMovies: moviesTrending)
                    
            case .failed( _):
                    self.view?.alertShow(title: "Error", error: "Hay un problema con la comunicación con el servidor, comprueba que tengas acceso a internet o intenta más tarde")
            }
            
        })

    }
    
    func prepareForRouter(movie: Result) {
        
        guard let id =  movie.id, let originalTitle = movie.originalTitle, let voteCount = movie.voteCount, let overview = movie.overview, let posterPath = movie.posterPath, let backdropPath = movie.backdropPath else {
            return
        }
        self.router.goToDetail(movie: ModelDataDetail(id: id, originalTitle: originalTitle, voteCount: voteCount, overview: overview, posterPath: posterPath, backdropPath: backdropPath))
    }
    
    func actionButtonRightBar() {
        self.router.goToLike()
    }
    
}
