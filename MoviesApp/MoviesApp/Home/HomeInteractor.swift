//
//  HomeInteractor.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import Foundation

enum ServiceResult{
    case success(data: Any?)
    case failed(error: Any?)
}

typealias ServiceCompletion = (_ result:ServiceResult) -> ()

protocol HomeUseCase {
    func fetchRequest(page: Int, callBack: @escaping ServiceCompletion)
}

class HomeInteractor {
    
    var manager = Manager()
    
}


extension HomeInteractor: HomeUseCase{
    
    func fetchRequest(page: Int, callBack: @escaping ServiceCompletion) {

        manager.get(endPonit: Endpoint.trendingMovies.rawValue, paramsData: ["page": "\(page)"], onSuccess: { data,status in
            
            let decoder = JSONDecoder()
            guard let decoderTrendingMovies = try? decoder.decode(Trending.self, from: data as! Data ) else{
                callBack(ServiceResult.failed(error: "Error con los datos del servidor"))
                return
            }
            callBack(ServiceResult.success(data: decoderTrendingMovies))
            
        }, onError: { error in
            callBack(ServiceResult.failed(error: error))
        })
        
    }
    
}
