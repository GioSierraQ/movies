//
//  HomeViewController.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import UIKit

protocol HomeView: class {
    func updateMovies(dataMovies: Trending)
    func alertShow(title:String, error:String)
}

class HomeViewController: UIViewController {

    @IBOutlet weak var collectionMoviesPoster: UICollectionView!
    
    var presenter: HomePresenter?
    var dataMovies: Trending?
    var movies: [Result] = []
    var isLoading = false
    static var page = 1
    var alert = Alerts()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(systemName: "heart"), style: .plain, target: self, action: #selector(actionButtonRightBar))
        
        self.title = "Movies"
        // Do any additional setup after loading the view.
        setupCollectionPoster()
        self.presenter?.listMovies(page: HomeViewController.page)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.prefersLargeTitles = true
    }
    
    @objc func actionButtonRightBar() {
        self.presenter?.actionButtonRightBar()
    }

}

extension HomeViewController: HomeView{
    
    func updateMovies(dataMovies: Trending) {
        guard let pageData = dataMovies.page, let moviesResult = dataMovies.results else {
            return
        }
        self.dataMovies = dataMovies
        self.movies.append(contentsOf: moviesResult)
        collectionMoviesPoster.reloadData()
        HomeViewController.page = pageData + 1

    }
    
    func alertShow(title:String, error:String){
        alert.ShowAlertSencilla(title: title, message: error, viewController: self)
    }
    
}

extension HomeViewController: UICollectionViewDelegate{
    
    func setupCollectionPoster(){
        collectionMoviesPoster.dataSource = self
        collectionMoviesPoster.delegate = self
        collectionMoviesPoster.register(UINib(nibName: "CollectionMoviesPosterViewCell", bundle: .main), forCellWithReuseIdentifier: "CollectionMoviesPosterViewCell")
    }
    
}

extension HomeViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionMoviesPosterViewCell", for: indexPath) as! CollectionMoviesPosterViewCell
        cell.setupCell(pathPosterImage: movies[indexPath.row].posterPath)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter?.prepareForRouter(movie: movies[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if indexPath.row == movies.count - 3 && !self.isLoading && (dataMovies?.totalPages != HomeViewController.page) {
                self.presenter?.listMovies(page: HomeViewController.page)
        }
    }
    
}

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = (collectionView.bounds.width/3.0) - 2
        let yourHeight = yourWidth + 50

        return CGSize(width: yourWidth, height: yourHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  2
    }
    
}
