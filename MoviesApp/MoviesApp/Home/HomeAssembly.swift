//
//  HomeAssembly.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import UIKit


class HomeAssembly {
    
    static func buidNavigation(usingNavFactory factory: NavigationFactory) -> UINavigationController {
        
        let storyboard = UIStoryboard(name: "Main", bundle: .main)
        let view = storyboard.instantiateViewController(withIdentifier: "HomeViewController") as! HomeViewController
        
        let interactor = HomeInteractor()
        let router = HomeRouter(view: view)
        let presenter = HomePresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        
        return factory(view)
        
    }
    
}
