//
//  Manager.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import Foundation
import Alamofire

// MARK: Enum for endpoints finish
enum Endpoint: String {
    case trendingMovies = "trending/movie/day"

}

class Manager {
    
    var API_KEY = "82a3e3a6fdcd75510f569625bd4decb3"
    
    // MARK: URL API MOVIES themoviedb.org // se debe enviar en url el dato get ?api_key=<<api_key>>
    var BASE_URL  = "https://api.themoviedb.org/3/"
    
    // MARK: Url para llamar imagenes
    var BASE_URL_IMAGE  = "https://image.tmdb.org/t/p/w500"
    
    
    // MARK: GET data para cualquier data traida del endpoint
    func get(endPonit: String, paramsData: [String : String]?, onSuccess: @escaping (Any, Int) -> Void, onError: @escaping(Any) -> Void)  {
        
        var params = ["api_key": API_KEY]
        paramsData?.forEach { params[$0] = $1 }
        
        AF.request(BASE_URL + endPonit, method: .get, parameters: params, encoding: URLEncoding.queryString, headers: nil, interceptor: nil, requestModifier: nil).responseData {
            response in
            
            switch response.result {
                case .success(let JSON):
                    
                    guard let statusCode = response.response?.statusCode else {
                        return
                    }
                    
                    if statusCode < 400 {
                        return onSuccess(JSON,statusCode)
                    }else{
                        let decoder = JSONDecoder()
                        guard let decodeResponseError = try? decoder.decode(ErrorResponse.self, from: JSON ) else{
                            return onError("Error desconocido")
                        }
                        return onError(decodeResponseError.statusMessage as Any)
                    }
            
                case .failure(let error):
                    return onError(error)
            }
            
        }
        
    }
    
}
