//
//  CollectionMoviesPosterViewCell.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

class CollectionMoviesPosterViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagePoster: UIImageView!
    
    var manager = Manager()
    
    override class func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func setupCell(pathPosterImage: String?) {
        
        guard let imagenPoster = pathPosterImage else{
            return
        }
        let _ = loadImage(urlImage: manager.BASE_URL_IMAGE + imagenPoster, ouletImage: imagePoster)

    }
    
    override func prepareForReuse() {
        self.imagePoster.image = nil
    }
        
}
