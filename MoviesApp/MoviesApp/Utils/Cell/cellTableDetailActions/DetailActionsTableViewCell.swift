//
//  DetailActionsTableViewCell.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

class DetailActionsTableViewCell: UITableViewCell {

    @IBOutlet weak var buttonLike: UIButton!
    @IBOutlet weak var labelVotos: UILabel!
    
    var managerInternal = ManagerInternal()
    static var estimatedheight: CGFloat = 50.0
    var movie: ModelDataDetail?
    let appDelegate = UIApplication.shared.delegate as? AppDelegate

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(movie: ModelDataDetail?){
        
        self.movie = movie
        
        guard let idMovie = movie?.id, let votos = movie?.voteCount else {
            return
        }
        labelVotos.text = "Votos: \(votos)"
        
        if managerInternal.loadLocalLikeMovieId(id: idMovie, appDelegate: appDelegate!){
            buttonLike.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            buttonLike.tintColor = .red
            buttonLike.tag = 2
        }
        
    }
    
    @IBAction func actionLike(_ sender: Any) {
       
        managerInternal.saveLocalLikeMovie(movieR: movie, appDelegate: appDelegate!)
        checkImageButton()

    }
    
    func checkImageButton() {
        if (buttonLike.tag == 2) {
            buttonLike.setImage(UIImage(systemName: "heart"), for: .normal)
            buttonLike.tintColor = .gray
            buttonLike.tag = 0
        }else{
            buttonLike.setImage(UIImage(systemName: "heart.fill"), for: .normal)
            buttonLike.tintColor = .red
            buttonLike.tag = 2
        }
    }
    
}
