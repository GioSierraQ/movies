//
//  DetailSumaryTableViewCell.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

class DetailSumaryTableViewCell: UITableViewCell {

    @IBOutlet weak var textSumary: UILabel!
    
    static var estimatedheight: CGFloat = 250.0

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(movie: ModelDataDetail?){
        
        guard let summary = movie?.overview else {
            return
        }
        
        textSumary.text = summary
        textSumary.sizeToFit()
        
        DetailSumaryTableViewCell.estimatedheight = CGFloat(textSumary.bounds.height + 60)
        
    }
    
}
