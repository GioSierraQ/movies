//
//  DetailImageTableViewCell.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

class DetailImageTableViewCell: UITableViewCell {

    @IBOutlet weak var imageDetailCell: UIImageView!
    
    var manager = Manager()
    
    static var estimatedheight: CGFloat = 250.0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func setup(movie: ModelDataDetail?) {
        
        if let imagenPoster = movie?.backdropPath {
            let _ = loadImage(urlImage: manager.BASE_URL_IMAGE + imagenPoster, ouletImage: imageDetailCell)
        }else{
            imageDetailCell.image = UIImage(systemName: "questionmark.folder")
        }
        
    }
    
}
