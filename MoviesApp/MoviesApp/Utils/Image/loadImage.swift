//
//  loadImage.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit
import Kingfisher

class loadImage {
    
    init(urlImage: String, ouletImage: UIImageView) {
        
        let url = URL(string: urlImage)
        let processor = DownsamplingImageProcessor(size: ouletImage.bounds.size)
                     |> RoundCornerImageProcessor(cornerRadius: 5)
        ouletImage.kf.indicatorType = .activity
        ouletImage.kf.setImage(
            with: url,
            placeholder: UIImage(named: "placeholderImage"),
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success(let value):
                print("Task done for: \(value.source.url?.absoluteString ?? "")")
            case .failure(let error):
                print("Job failed: \(error.localizedDescription)")
            }
        }
        
    }

}
