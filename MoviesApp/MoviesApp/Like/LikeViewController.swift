//
//  LikeViewController.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

protocol LikeView: class {
    func updateMoviesLike(dataMovies: [AnyObject])
}

class LikeViewController: UIViewController {

    @IBOutlet weak var collectionMovieLike: UICollectionView!
    
    var presenter: LikePresenter?
    var moviesLike: [AnyObject] = []
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "Likes Movies"
        setupCollectionMovieLike()
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
        
        self.presenter?.listMoviesLike(appDelegate: appDelegate!)
        // Do any additional setup after loading the view.
    }

}

extension LikeViewController: LikeView {
    
    func updateMoviesLike(dataMovies: [AnyObject]) {

        self.moviesLike = dataMovies
        collectionMovieLike.reloadData()

    }
}

extension LikeViewController: UICollectionViewDelegate{
    
    func setupCollectionMovieLike(){
        collectionMovieLike.dataSource = self
        collectionMovieLike.delegate = self
        collectionMovieLike.register(UINib(nibName: "CollectionMoviesPosterViewCell", bundle: .main), forCellWithReuseIdentifier: "CollectionMoviesPosterViewCell")

    }
    
}

extension LikeViewController: UICollectionViewDataSource{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        moviesLike.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CollectionMoviesPosterViewCell", for: indexPath) as! CollectionMoviesPosterViewCell
        cell.setupCell(pathPosterImage: (moviesLike[indexPath.row] as AnyObject).value(forKeyPath: "posterPath") as? String)
        
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.presenter?.prepareForRouter(data: moviesLike[indexPath.row] as AnyObject)
    }
    
}

extension LikeViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let yourWidth = (collectionView.bounds.width/3.0) - 2
        let yourHeight = yourWidth + 50

        return CGSize(width: yourWidth, height: yourHeight)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return  10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return  2
    }
    
}
