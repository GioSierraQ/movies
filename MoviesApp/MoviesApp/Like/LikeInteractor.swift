//
//  LikeInteractor.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import Foundation

protocol LikeUseCase {
    func fetchMoviesLike(appDelegate: AppDelegate, callBack: @escaping ServiceCompletion)
}

class LikeInteractor {
    
    var managerInternal = ManagerInternal()


}

extension LikeInteractor: LikeUseCase {
    func fetchMoviesLike(appDelegate: AppDelegate, callBack: @escaping ServiceCompletion) {
        guard let data = managerInternal.loadLocalLikeMovie(appDelegate: appDelegate) else{
            callBack(ServiceResult.failed(error: "Error con los datos guardados"))
            return
        }
        callBack(ServiceResult.success(data: data))

    }
    
    
}
