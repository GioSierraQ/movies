//
//  LikePresenter.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import Foundation

protocol LikePresenting {
    func viewDidLoad()
    func listMoviesLike(appDelegate: AppDelegate)
    func prepareForRouter(data: AnyObject)
}

class LikePresenter {
    
    weak var view: LikeView?
    var interactor: LikeUseCase
    var router: LikeRouter
    
    init(view: LikeView, interactor: LikeUseCase, router: LikeRouter) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
}

extension LikePresenter: LikePresenting {
    
    func viewDidLoad() {
        
    }
    
    func listMoviesLike(appDelegate: AppDelegate) {
        self.interactor.fetchMoviesLike(appDelegate: appDelegate, callBack: { (result) in
            
            switch result {
            
                case .success(let data):
                    self.view?.updateMoviesLike(dataMovies: data as! [AnyObject])
                case .failed(let error):
                    print("\(String(describing: error))")
                    
            }
            
        })
    }
    
    func prepareForRouter(data: AnyObject){
        
            guard let id =  data.value(forKeyPath: "id") as? Int, let originalTitle = data.value(forKeyPath: "originalTitle") as? String, let voteCount = data.value(forKeyPath: "voteCount") as? Int, let overview = data.value(forKeyPath: "overview") as? String, let posterPath = data.value(forKeyPath: "posterPath") as? String, let backdropPath = data.value(forKeyPath: "backdropPath") as? String else {
                return
            }
            self.router.goToDetail(movie: ModelDataDetail(id: id, originalTitle: originalTitle, voteCount: voteCount, overview: overview, posterPath: posterPath, backdropPath: backdropPath))

    }
    
}
