//
//  LikeRouter.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

protocol LikeRouting {
    func goToDetail(movie: ModelDataDetail)
}

class LikeRouter {
    
    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
    
}

extension LikeRouter: LikeRouting {

    
    func goToDetail(movie: ModelDataDetail) {
        let vc = DetailAssembly.buid(movie: movie)
        self.view.navigationController?.pushViewController(vc, animated: true)
    }
    
    
}
