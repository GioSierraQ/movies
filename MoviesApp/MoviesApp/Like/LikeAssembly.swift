//
//  LikeAssembly.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

class LikeAssembly {
    
    static func build() -> UIViewController {
        
        let view = LikeViewController(nibName: "LikeViewController", bundle: .main)
        
        let Interactor = LikeInteractor()
        let router = LikeRouter(view: view)
        view.presenter = LikePresenter(view: view, interactor: Interactor, router: router)

        return view
    }
    
}
