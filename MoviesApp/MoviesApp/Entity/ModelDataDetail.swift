//
//  ModelDataDetail.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import Foundation

struct ModelDataDetail: Codable {
    
    var id: Int?
    var originalTitle: String?
    var voteCount: Int?
    var overview: String?
    var posterPath: String?
    var backdropPath: String?

    init(id: Int, originalTitle: String, voteCount: Int, overview: String, posterPath: String, backdropPath: String) {
        self.id = id
        self.originalTitle = originalTitle
        self.voteCount = voteCount
        self.overview = overview
        self.posterPath = posterPath
        self.backdropPath = backdropPath
    }
}
