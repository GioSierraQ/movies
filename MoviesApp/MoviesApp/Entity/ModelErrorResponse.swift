//
//  ModelErrorResponse.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 31/07/21.
//

import Foundation

import Foundation

// MARK: - Trending
struct ErrorResponse: Codable {
    let statusMessage: String?
    let success: Bool?
    let statusCode: Int?

    enum CodingKeys: String, CodingKey {
        case statusMessage = "status_message"
        case success
        case statusCode = "status_code"
    }
}
