//
//  ManagerInternal.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import Foundation
import CoreData

class ManagerInternal {
    
    var movieLike: [NSManagedObject] = []

    func saveLocalLikeMovie(movieR: ModelDataDetail?, appDelegate: AppDelegate ) -> Bool{
        
        let managedContext = appDelegate.persistentContainer.viewContext

        if !loadLocalLikeMovieId(id: movieR?.id! ?? 0, appDelegate: appDelegate) {
        
            guard let movie = movieR else {
                return false
            }
            

            let entity = NSEntityDescription.entity(forEntityName: "MovieLike", in: managedContext)!

            let movieObject = NSManagedObject(entity: entity, insertInto: managedContext)

            movieObject.setValue(movie.id, forKeyPath: "id")
            movieObject.setValue(movie.originalTitle, forKeyPath: "originalTitle")
            movieObject.setValue(movie.voteCount, forKeyPath: "voteCount")
            movieObject.setValue(movie.overview, forKeyPath: "overview")
            movieObject.setValue(movie.posterPath, forKeyPath: "posterPath")
            movieObject.setValue(movie.backdropPath, forKeyPath: "backdropPath")

            do {
                try managedContext.save()
                movieLike.append(movieObject)
                return true
            } catch let error as NSError {
                print("Could not save. \(error), \(error.userInfo)")
                return false
            }
            
        }else{
            
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieLike")
            fetchRequest.predicate = NSPredicate(format: "id == %d", movieR?.id! ?? 0)
            do {
                let objects = try managedContext.fetch(fetchRequest)
                for object in objects {
                    managedContext.delete(object as! NSManagedObject)
                }
                try managedContext.save()
                return true
            } catch _ {
                return false
            }
        }


    }
    
    func loadLocalLikeMovie(appDelegate: AppDelegate) -> [NSManagedObject]? {
        
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieLike")
        request.returnsObjectsAsFaults = false
       
        let managedContext = appDelegate.persistentContainer.viewContext

        guard let result = try? managedContext.fetch(request) else{
            return nil
        }
        
        return result as? [NSManagedObject]
         
    }
    
    func loadLocalLikeMovieId(id: Int, appDelegate: AppDelegate) -> Bool {
        
        let managedContext = appDelegate.persistentContainer.viewContext

        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "MovieLike")
        request.predicate = NSPredicate(format: "id contains[c] %@", String(id))
        request.returnsObjectsAsFaults = false

        do {
            
            let finMovieId = try managedContext.fetch(request)
            if finMovieId.count != 0 {
                return true
            }else{
                return false
            }
            
        } catch {
            return false
        }
         
    }
    
    
}
