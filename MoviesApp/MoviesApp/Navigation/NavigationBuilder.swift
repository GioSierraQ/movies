//
//  NavigationBuilder.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

typealias NavigationFactory = (UIViewController) -> (UINavigationController)

class NavigationBuilder {
    
    static func build(rootview: UIViewController) -> UINavigationController {
        
        let navController = UINavigationController(rootViewController: rootview)
                
        return navController
    }
    
}
