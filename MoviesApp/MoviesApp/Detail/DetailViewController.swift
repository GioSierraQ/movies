//
//  DetailViewController.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

enum CellDetails {
    case image
    case actions
    case sumary
}

protocol DetailView: class {
    func updateMovie(movie: ModelDataDetail)

}

class DetailViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var movie: ModelDataDetail?
    var presenter: DetailPresenter?
    
    private let cellTypes = [CellDetails.image, CellDetails.actions, CellDetails.sumary]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.prefersLargeTitles = false

        setupTableView()
        
        guard let titleMovie = movie?.originalTitle else {
            return
        }
        self.title = titleMovie
        // Do any additional setup after loading the view.
    }

}

extension DetailViewController: DetailView {
    func updateMovie(movie: ModelDataDetail) {
        self.movie = movie
    }
    
    
}

extension DetailViewController: UITableViewDelegate{
    
    func setupTableView() {
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.tableView.register(UINib(nibName: "DetailImageTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailImageTableViewCell")
        self.tableView.register(UINib(nibName: "DetailActionsTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailActionsTableViewCell")
        self.tableView.register(UINib(nibName: "DetailSumaryTableViewCell", bundle: nil), forCellReuseIdentifier: "DetailSumaryTableViewCell")
    }
    
}

extension DetailViewController: UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        cellTypes.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch cellTypes[indexPath.row] {
        case .image:
            return cellDetailImageTable(tableView: tableView, indexPath: indexPath)
        case .actions:
            return cellDetailActionsTable(tableView: tableView, indexPath: indexPath)
        case .sumary:
            return cellDetailSumaryTable(tableView: tableView, indexPath: indexPath)
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch cellTypes[indexPath.row] {
        case .image:
            return DetailImageTableViewCell.estimatedheight
        case .actions:
            return DetailActionsTableViewCell.estimatedheight
        case .sumary:
            return DetailSumaryTableViewCell.estimatedheight
        }
    }
    
    private func cellDetailImageTable(tableView: UITableView, indexPath: IndexPath) -> DetailImageTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailImageTableViewCell", for: indexPath) as! DetailImageTableViewCell
        cell.setup(movie: movie)
        return cell
    }
    
    private func cellDetailActionsTable(tableView: UITableView, indexPath: IndexPath) -> DetailActionsTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailActionsTableViewCell", for: indexPath) as! DetailActionsTableViewCell
        cell.setup(movie: movie)
        return cell
    }
    
    private func cellDetailSumaryTable(tableView: UITableView, indexPath: IndexPath) -> DetailSumaryTableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "DetailSumaryTableViewCell", for: indexPath) as! DetailSumaryTableViewCell
        cell.setup(movie: movie)
        return cell
    }
}
