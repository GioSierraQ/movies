//
//  DetailPresenter.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import Foundation

protocol DetailPresentation: class {
    func viewDidLoad()
}

class DetailPresenter {
    
    weak var view: DetailView?
    var interactor: DetailUseCase
    var router: DetailRouting
    
    init(view: DetailView, interactor: DetailUseCase, router: DetailRouting) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
    
}

extension DetailPresenter: DetailPresentation {
    
    func viewDidLoad() {
        
    }
    
    
}
