//
//  DetailAssembly.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

class DetailAssembly {
    
    static func buid(movie: ModelDataDetail) -> UIViewController {
        
        let view = DetailViewController(nibName: "DetailViewController", bundle: .main)
        
        let interactor = DetailInteractor()
        let router = DetailRouter(view: view)
        let presenter = DetailPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        view.updateMovie(movie: movie)
        
        return view
        
    }
    
}
