//
//  DetailRouter.swift
//  MoviesApp
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import UIKit

protocol DetailRouting {
    
}

class DetailRouter {
    
    var view: UIViewController
    
    init(view: UIViewController) {
        self.view = view
    }
    
}

extension DetailRouter: DetailRouting {
    
}
