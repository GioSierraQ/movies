//
//  InternalDataTest.swift
//  MoviesAppTests
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import XCTest

@testable import MoviesApp

class InternalDataTest: XCTestCase {
    
    var suc: ManagerInternal!
        
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        suc = ManagerInternal()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_Save_Data_Local() throws {
        
        //Given
        let url = ModelDataDetail(id: 66, originalTitle: "Prueba", voteCount: 2, overview: "Contenido de prueba", posterPath: "/pathdeprueba.jpg", backdropPath: "/pathdeprueba.jpg")
        let appDelegate = UIApplication.shared.delegate as? AppDelegate

        //When
        let saveStatus = suc.saveLocalLikeMovie(movieR: url, appDelegate: appDelegate!)
        
        //then
        XCTAssertTrue(saveStatus)
        
    }
    
    func test_Load_All_Data_Local() throws {
        
        //Given
        let appDelegate = UIApplication.shared.delegate as? AppDelegate

        //When
        let saveStatus = suc.loadLocalLikeMovie(appDelegate: appDelegate!)
        
        //then
        XCTAssertNotNil(saveStatus)
        
    }
    
    func test_Load_One_Movie_Data_Local() throws {
        
        //Given
        let Movie = 2
        let appDelegate = UIApplication.shared.delegate as? AppDelegate

        //When
        let saveStatus = suc.loadLocalLikeMovieId(id: Movie, appDelegate: appDelegate!)
        
        //then
        XCTAssertNotNil(saveStatus)
        
    }
    
}
