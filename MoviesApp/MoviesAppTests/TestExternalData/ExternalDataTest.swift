//
//  ExternalDataTest.swift
//  MoviesAppTests
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import XCTest

@testable import MoviesApp

class ExternalDataTest: XCTestCase {
    
    var suc: Manager!
        
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        suc = Manager()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func test_Url_Errada() throws {
        
        //Given
        let url = "/noexiste/"
        let expectation = self.expectation(description: "Url_Errada")
        
        //When
        suc.get(endPonit: url, paramsData: nil, onSuccess: { data, status in
            
            //Then
            XCTAssertNotNil(data)
            XCTAssertNotNil(status)
            XCTAssertEqual(200, status)
            expectation.fulfill()
            
        }, onError: { error in
            
            //Then
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
    func test_Url_API_mal() throws {
        
        //Given
        let url = "trending/movie/day?api_key=324234wwe"
        let expectation = self.expectation(description: "Url_API_mal")
        
        //When
        suc.get(endPonit: url, paramsData: nil, onSuccess: { data, status in
            
            //Then
            XCTAssertNotNil(data)
            XCTAssertNotNil(status)
            XCTAssertEqual(200, status)
            expectation.fulfill()
            
        }, onError: { error in
            
            //Then
            XCTAssertNotNil(error)
            expectation.fulfill()
        })
        
        waitForExpectations(timeout: 5, handler: nil)
        
    }
    
}
