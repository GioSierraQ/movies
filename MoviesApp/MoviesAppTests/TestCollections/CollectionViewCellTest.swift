//
//  CollectionViewCellTest.swift
//  MoviesAppTests
//
//  Created by Giovani Sierra quintero on 1/08/21.
//

import XCTest

@testable import MoviesApp


class CollectionViewCellTest: XCTestCase {
    
    var suc: CollectionMoviesPosterViewCell!
        
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        suc = CollectionMoviesPosterViewCell()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testDatoSetupNil() throws {
        
        //Given
        //When
        //Then
        let pathPosterImage: String? = nil
        XCTAssertNoThrow(suc.setupCell(pathPosterImage: pathPosterImage))
    }
    
}
